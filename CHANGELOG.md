# Changelog

All notable changes to this project will be documented in this file.

## [0.1.2](https://gitlab.com/biomedit/tools/danger/-/releases/0.1.2) - 2023-09-19

[See all changes since the last release](https://gitlab.com/biomedit/tools/danger/compare/0.1.1...0.1.2)


### 🐞 Bug Fixes

- Fix checkForbiddenWords ([f3c676f](https://gitlab.com/biomedit/tools/danger/commit/f3c676ff996527eb51ecb24289d4073bd8012920))

## [0.1.1](https://gitlab.com/biomedit/tools/danger/-/releases/0.1.1) - 2023-09-19

[See all changes since the last release](https://gitlab.com/biomedit/tools/danger/compare/0.1.0...0.1.1)


### ✨ Features

- Add checkForbiddenWords ([418812d](https://gitlab.com/biomedit/tools/danger/commit/418812d571902da1b1e15c633728a12adf3adca9))

### 🧱 Build system and dependencies

- Switch from standard-version to git-cliff ([5cd579f](https://gitlab.com/biomedit/tools/danger/commit/5cd579ffe5cb70c18a69852b8afa2d0c7a43b805))
- Update dependencies ([8f45797](https://gitlab.com/biomedit/tools/danger/commit/8f457972258d108a5a75a87117ab8a2d3ad3dd2a))

### 👷 CI

- Remove dependency on deprecated templates ([48157cb](https://gitlab.com/biomedit/tools/danger/commit/48157cb6fa14db94f428be0d27db5a5a5e128dbb))
# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/biomedit/tools/danger/compare/0.0.1...0.1.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **notifyTechnicalCoordinators:** `notifyTechnicalCoordinators` doesn't accept a `technicalCoordinator` parameter
anymore. Also, `message`, `warn`, and `fail` are not exported anymore, as users can easily import
them directly from `danger` in their Dangerfile.

### Features

* **ci/danger:** add danger to the project ([430385a](https://gitlab.com/biomedit/tools/danger/commit/430385a5cd716e5fa7877fb2e460fbd1724f3d68))
* **notifyTechnicalCoordinators:** pick up technical coordinator from env ([9847cff](https://gitlab.com/biomedit/tools/danger/commit/9847cffa6a42dddc8e42951c30b886c0de67d654)), closes [#1](https://gitlab.com/biomedit/tools/danger/issues/1)

### 0.0.1 (2022-11-28)


### Features

* initial commit ([d7377c5](https://gitlab.com/biomedit/tools/danger/commit/d7377c51c4dedef39aa7a8d5593d3e0cdb955bed))
