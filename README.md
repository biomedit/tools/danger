# @biomedit/danger

Useful Danger tasks for the BioMedIT project.

## Release

To release a new version of the package:

```bash
git checkout main
git pull
./bumpversion.sh
git push --follow-tags origin main
```
