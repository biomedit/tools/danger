#!/usr/bin/env bash

set -e

release_type="$1"
node_pkg_version_script="console.log(JSON.parse(require('fs').readFileSync(process.argv[1]))['version'])"
# The `sed` in-place command behaves differently between the BSD (MacOS) and GNU (Linux)
# implementations. This makes the command portable.
# Default case for Linux sed, just use "-i"
sedi=(-i)
case "$(uname)" in
  # For MacOS, use two parameters
  Darwin*) sedi=(-i "") ;;
esac

version_current=$(node -e "${node_pkg_version_script}" -- ./package.json)
echo "📌 Current version: $version_current" >&2
changelog_unreleased=$(git cliff -c .cliff.toml --unreleased)
case $changelog_unreleased in
*"BREAKING CHANGES"*)
    version_bump_recommended="major"
    ;;
*"### Features"*)
    version_bump_recommended="minor"
    ;;
*)
    version_bump_recommended="patch"
    ;;
esac
echo "⚙️  Bumping to the next $version_bump_recommended version" >&2
npm version --no-git-tag-version "$version_bump_recommended" >/dev/null
version_new=$(node -e "${node_pkg_version_script}" -- package.json)
echo "📜 Generating changelog" >&2
git cliff -c .cliff.toml -u -p CHANGELOG.md --tag "$version_new" >&2
git add CHANGELOG.md
git add ./package.json ./package-lock.json
commit_msg="chore(release): ${version_new}"
git commit -q -m "$commit_msg"
echo "🖊️  New commit: $commit_msg" >&2
git tag -a "$version_new" -m "$commit_msg"
echo "🏷️  New tag: $version_new" >&2
echo "🚀 Version bumped to: $version_new" >&2
echo "👷 You can now push the new version using: git push --follow-tags origin $(git branch --show-current)" >&2
echo "$version_new"
