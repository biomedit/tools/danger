import { schedule } from 'danger';
import {
  checkBreakingChangeFooter,
  checkForbiddenWords,
  notifyTechnicalCoordinators,
  praiseDependencyReduction,
} from './src';

checkBreakingChangeFooter();
notifyTechnicalCoordinators();
praiseDependencyReduction('./package.json');
schedule(checkForbiddenWords);
