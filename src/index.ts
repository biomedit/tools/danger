// Provides dev-time type structures for  `danger` - doesn't affect runtime.
import { DangerDSLType } from '../node_modules/danger/distribution/dsl/DangerDSL';
import process from 'process';

declare var danger: DangerDSLType;
declare function message(message: string): void;
declare function warn(message: string): void;
declare function fail(message: string): void;

export async function praiseDependencyReduction(pkgFile: string) {
  if (danger.git.modified_files.includes(pkgFile)) {
    const displayMsg = (depList: string[] | undefined, isDev: boolean) => {
      if (depList && depList.length > 0) {
        message(
          ':tada: Congratulations! You have reduced the ' +
            (isDev ? 'development ' : '') +
            'dependency tree by ' +
            `${depList.length} ` +
            (depList.length == 1 ? 'package' : 'packages') +
            ` (${depList})`,
        );
      }
    };
    const result = await danger.git.JSONDiffForFile(pkgFile);
    displayMsg(result['dependencies']?.removed, false);
    displayMsg(result['devDependencies']?.removed, true);
  }
}

export function warnNoTests(
  srcTargets: string[],
  testTargets: string[],
  where: string,
) {
  if (
    danger.git.fileMatch(...srcTargets).edited &&
    !danger.git.fileMatch(...testTargets).edited
  ) {
    warn(
      `(${where}): There are changes in the application code but not in tests. ` +
        "That's OK as long as you're refactoring existing code.",
    );
  }
}

export function notifyTechnicalCoordinators(labelName = 'UX/UI') {
  const technicalCoordinator = process.env.TECHNICAL_COORDINATOR;
  if (danger.gitlab.mr.labels.includes(labelName) && technicalCoordinator) {
    message(
      `Tagging ${technicalCoordinator} because this MR was labelled with ${labelName}.`,
    );
  }
}

export function checkBreakingChangeFooter(
  labelName = 'breaking-change',
  footer = 'BREAKING CHANGE:',
) {
  const breakingCommits = danger.git.commits.filter((commit) =>
    commit.message.includes(footer),
  );

  if (
    danger.gitlab.mr.labels.includes(labelName) &&
    !!danger.git.commits.length &&
    !breakingCommits.length
  ) {
    fail(
      `This MR was labelled with ${labelName}, but none of its commits have the \`${footer}\` footer.`,
    );
  }
}

export async function checkForbiddenWords() {
  const forbiddenWords = process.env.FORBIDDEN_WORDS?.split(':');
  const promises = danger.git.modified_files.map(danger.git.diffForFile);
  Promise.all(promises)
    .then((textDiffs) => {
      if (
        textDiffs.some((textDiff) => {
          const added = textDiff?.added.toLocaleLowerCase();
          return forbiddenWords?.some(
            (forbiddenWord) => added?.includes(forbiddenWord),
          );
        })
      ) {
        warn(
          '🙊 Your code changes contain one or more words that are part of your ' +
            '`FORBIDDEN_WORDS` CI variable.',
        );
      }
    })
    .catch((reason) =>
      message(`💥 Error while fetching the code changes: ${reason}`),
    );
}
